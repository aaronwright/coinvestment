from flask import Flask
from flask_restful import Api
from flask_jwt_extended import JWTManager

#from security import authenticate, identity
from resources.user import UserRegister, User, UserLogin, CurrentUser
from resources.coin import CoinList
from resources.investment import InvestmentList

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True # To allow flask propagating exception even if debug is set to false on app
app.secret_key = 'asdfasdf'
api = Api(app)


jwt = JWTManager(app)


@app.before_first_request
def create_tables():
    db.create_all()


api.add_resource(UserRegister, '/register')
api.add_resource(User, '/user/<int:user_id>')
api.add_resource(UserLogin, '/api/login')
api.add_resource(CurrentUser, '/api/current-user')
api.add_resource(CoinList, '/api/all-coins')
api.add_resource(InvestmentList, '/api/investments')

if __name__ == '__main__':
    from db import db
    db.init_app(app)
    app.run(debug=True)  # important to mention debug=True
