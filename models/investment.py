import sqlite3
from db import db


class InvestmentModel(db.Model):
    __tablename__ = 'investments'
    id = db.Column(db.Integer, primary_key=True)
    coin_amount = db.Column(db.Float(7))
    bought_at = db.Column(db.Float())

    coin_id = db.Column(db.Integer, db.ForeignKey('coins.id'))
    coin = db.relationship('CoinModel')
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    user = db.relationship('UserModel')

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


    def json(self):
        return {
            'id': self.id, 
            'bought_at': self.bought_at,
            'amount': self.coin_amount,
            'coin_name': self.coin.name,
            'difference': (self.coin.current_price * self.coin_amount)  - (self.coin_amount * self.bought_at),
            'current_val': self.coin.current_price * self.coin_amount                
            }



