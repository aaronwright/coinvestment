import sqlite3
from db import db


class CoinModel(db.Model):
    __tablename__ = 'coins'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150))
    current_price = db.Column(db.Float)
    description = db.Column(db.String(300))


    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


    def json(self):
        return {
        'id': self.id,
        'name': self.name,
        'current_price': self.current_price,
        'description': self.description
        }

