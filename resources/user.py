import sqlite3
from flask_restful import Resource, reqparse
from werkzeug.security import safe_str_cmp
from flask_jwt_extended import create_access_token, create_refresh_token, get_jwt_identity, jwt_optional, jwt_refresh_token_required
from models.user import UserModel
from flask import request, Response


_user_parser = reqparse.RequestParser()
_user_parser.add_argument('username',
    type=str,
    required=True,
    help="This field cannot be blank"
)

_user_parser.add_argument('password',
    type=str,
    required=True,
    help="This field cannot be blank"
)

class UserRegister(Resource):
    def post(self):
        data = _user_parser.parse_args()

        if UserModel.find_by_username(data['username']):
            return {"message": "A user with that username already exists"}, 400

        user = UserModel(data['username'], data['password'])
        user.save_to_db()
        return {"message": "User created successfully"}, 201


class User(Resource):
    @classmethod
    def get(cls, user_id):

        user = UserModel.find_by_id(user_id)
        if not user:
            return {'message': 'User not found'}, 404
        return user.json()

    @classmethod
    def delete(cls, user_id):
        user = UserModel.find_by_id(user_id)

        if not user:
            return {'message': 'User not found'}, 404
        user.delete_from_db()
        return {'message': 'User deleted'}, 200


class UserLogin(Resource):
    @classmethod
    def post(cls):
        #get data from parser
        print(request.data)
        data = _user_parser.parse_args()
        print('DATA: {}'.format(data))
        # find user in database
        user = UserModel.find_by_username(data['username'])
        # check password
        if user and safe_str_cmp(user.password, data['password']):
            # create access token
            access_token = create_access_token(identity=user.id, fresh=True)
            # create refresh token
            refresh_token= create_refresh_token(identity=user.id)
            # return them
            return {
                'access_token': access_token,
                'refresh_token': refresh_token
            }, 200
        return {'message': 'Invalid credentials'}, 401


class CurrentUser(Resource):
    @jwt_optional
    def get(self):
        user_id = get_jwt_identity()
        print('USER ID: {}'.format(user_id))
        found_user = UserModel.find_by_id(user_id)
        if found_user:
            return found_user.json()
        return None




class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
