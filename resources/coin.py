import sqlite3
from flask_restful import Resource, reqparse

from models.coin import CoinModel
from flask import request, Response

class CoinList(Resource):

    def get(self):

        # btc = CoinModel()
        # btc.name = 'Monero'
        # btc.current_price = 122.04
        # btc.description = "The top privacy coin that obfuscates who transacted and how much was sent."
        # btc.save_to_db()
        return {'coins': [coin.json() for coin in CoinModel.query.all()]}