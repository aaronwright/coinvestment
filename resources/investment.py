from flask_restful import Resource, reqparse

from models.investment import InvestmentModel
from flask_jwt_extended import get_jwt_identity, jwt_optional

class InvestmentList(Resource):
    @jwt_optional
    def get(self):
        # get_jwt_identity either returns a user id, or returns None
        user_id = get_jwt_identity()
        if user_id:
            coins_purchased = []
            aggregate_totals = {'total_invested': 0, 
                                'current_value': 0, 
                                'difference': 0
                                }
            aggregate_coin_values = []
            investments = [inv.json() for inv in InvestmentModel.query.filter_by(user_id=user_id)]
            for inv in investments:
                coins_purchased.append(inv['coin_name'])
                aggregate_totals['total_invested'] += (inv['bought_at'] * inv['amount'])
                aggregate_totals['current_value'] += inv['current_val']
                aggregate_totals['difference'] += inv['difference']

            coins_purchased = list(set(coins_purchased))

            print(coins_purchased)

            for coin in coins_purchased:
             aggregate_coin_values.append({'name': coin,
                                            'amount': 0,
                                            'value': 0})

            for investment in investments:
                for agg_val in aggregate_coin_values:
                    if (investment['coin_name'] == agg_val['name']):
                        agg_val['amount'] += investment['amount']
                        agg_val['value'] += investment['current_val']

            print(aggregate_totals)


            data = {
                'totals': aggregate_totals,
                'coins': aggregate_coin_values,
                'allTrades': investments
            }


            return {'data': data}, 200

        return 400