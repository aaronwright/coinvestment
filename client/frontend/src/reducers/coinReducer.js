import { GET_ALL_COINS } from '../actions/types';

export default function(state={}, action) {
    switch (action.type) {
        case GET_ALL_COINS:
            return {
              coins: action.payload
            };
        default:
            return state;
    }
}
