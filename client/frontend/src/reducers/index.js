
import { combineReducers } from 'redux';
import authReducer from './authReducer';
import tokenReducer from './tokenReducer';
import investmentReducer from './investmentReducer';
import coinReducer from './coinReducer'


export default combineReducers({
        auth: authReducer,
        token: tokenReducer,
        investments: investmentReducer,
        coins: coinReducer
});
