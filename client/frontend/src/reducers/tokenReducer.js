import { SET_TOKEN } from '../actions/types';

export default function(state={}, action) {
    switch (action.type) {
        case SET_TOKEN:
      //       return Object.assign({}, state, {
      //   token: action.payload
      // });
            return {
              token: action.payload
            };
        default:
            return state;
    }
}
