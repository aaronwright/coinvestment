import { GET_INVESTMENTS, ADD_TRADE } from '../actions/types';

export default function(state={}, action) {
    switch (action.type) {
        case GET_INVESTMENTS:
            return {
              investments: action.payload
            };
        case ADD_TRADE:
            //state.investments.aggregate.totalInvested = state.investments.aggregate.totalInvested + action.payload.currentValue;
            const trade = action.payload;
            return {
                investments: {
                    totals: {
                        total_invested: state.investments.totals.total_invested + (trade.amount * trade.boughtAt),
                        current_value: state.investments.totals.current_value + trade.current_val,
                        difference: state.investments.totals.difference + trade.difference
                    },
                    coins: state.investments.coins.map((coin) => {
                        if (coin.name === trade.name) {
                            //there is already a coin aggregated
                            return {
                                name: coin.name,
                                amount: coin.amount + trade.amount,
                                price: coin.price + trade.difference
                            }
                        } else {
                            return coin;
                        }
                    }),  
                    allTrades: state.investments.allTrades.concat(action.payload)
                }
            }
        default:
            return state;
    }
}
