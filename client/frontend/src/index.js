import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
//import 'materialize-css/dist/css/materialize.min.css';
import reduxThunk from 'redux-thunk';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';

const store = createStore(reducers, applyMiddleware(reduxThunk));


ReactDOM.render(
<Provider store={store}><App/></Provider>,
document.querySelector('#root'));
