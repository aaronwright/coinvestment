export const FETCH_USER = 'fetch_user';

export const SET_TOKEN = 'set_token';

export const GET_INVESTMENTS = 'get_investments';

export const GET_ALL_COINS = 'get_all_coins';

export const ADD_TRADE = 'add_trade';
