import axios from 'axios';
import { FETCH_USER, SET_TOKEN, GET_INVESTMENTS, GET_ALL_COINS, ADD_TRADE } from './types';

export const fetchUser = () => async dispatch => {

        const token = localStorage.getItem('access_token');
        var authToken = 'Bearer ';

        if (token) {
          authToken += token;
        } else {
          console.log('NO TOKEN IN FETCH USER');
          return;
        }

        const res = await axios.get('/api/current-user',
                                    {headers: {
                                    "Authorization" : authToken
                                        }
                                      });
        dispatch({type: FETCH_USER, payload: res.data});
    }



export const setToken = (userData) => async dispatch => {
        const res = await axios({
         method: 'post',
         url: '/api/login',
         data: {"username": userData.username, "password": userData.password},
         config: { headers: {'Content-Type': 'application/json' }}
       });

       localStorage.setItem('access_token', res.data.access_token);
       localStorage.setItem('options', res.data.refresh_token);
      
       dispatch(fetchUser());
       dispatch(getInvestments());
       dispatch({type: SET_TOKEN, payload: res.data.access_token});
    }



export const getInvestments = () => async dispatch => {

        const token = localStorage.getItem('access_token');
        var authToken = 'Bearer ';

        if (token) {
          authToken += token;
        }

        const res = await axios.get('/api/investments',
                                    {headers: {
                                    "Authorization" : authToken
                                        }
                                      });


        dispatch({type: GET_INVESTMENTS, payload: res.data.data});
    }



    export const getAllCoins = () => async dispatch => {

            const res = await axios.get('/api/all-coins');

            dispatch({type: GET_ALL_COINS, payload: res.data.coins});
        }


export const addTrade = (tradeData) => async dispatch => {
       //  const res = await axios({
       //   method: 'post',
       //   url: '/api/login',
       //   data: {"username": userData.username, "password": userData.password},
       //   config: { headers: {'Content-Type': 'application/json' }}
       // });

       // Make request, this will come back
       const trade = {
                     coin_name: tradeData.name,
                     amount: tradeData.amount,
                     boughtAt: tradeData.price,
                     difference: 44.23,
                     current_val: tradeData.amount * tradeData.price
                     }
                


       dispatch({type: ADD_TRADE, payload: trade});
    }
