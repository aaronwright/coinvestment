import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';


 class Login extends Component {
   constructor(props) {
    super(props);

    this.onFormSubmit = this.onFormSubmit.bind(this);

    this.state = {
      error: undefined
    }
}


   onFormSubmit(e) {
     e.preventDefault();
     const name = e.target.elements.username.value.trim();
     const pw = e.target.elements.password.value;

     const userData = {'username': name, 'password': pw};

     this.props.setToken(userData);
   }



   render() {
     return (
         <div style={{textAlign: 'center'}}>
             <h1>
                 LOGIN
             </h1>
             <form onSubmit={this.onFormSubmit}>
                 <input type="text" name="username"/>
                 <input type="password" name="password"/>
                 <input type="submit" value="submit" />
             </form>
         </div>
     );
   }
}


export default connect(null, actions)(Login);
