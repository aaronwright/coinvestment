import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import * as actions from '../actions';




class Header extends Component {

    renderContent() {
        switch(this.props.auth) {
          case null:
              return;
          case false:
              return <li><a href="/login">Login</a></li>;
          default:  
            return <li className="nav-item"><a className="nav-link" href="/api/logout">Logged in as {this.props.auth}</a></li>;
              
        }
    }

    render() {
        return (
    <nav className="navbar navbar-expand-lg navbar-dark" style={{backgroundColor:'#210421'}}>
    <Link className="navbar-brand" to={this.props.auth ? '/dashboard' : '/'}>COINVESTMENT</Link>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/login">LOGIN</Link>
          </li>
          {this.renderContent()}
        </ul>
    </div>
    </nav>
        );
    }
}

function mapStateToProps(state) {
    return {auth: state.auth };
}

export default connect(mapStateToProps, actions)(Header);
