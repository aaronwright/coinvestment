import React, { Component } from 'react';



const DashboardAggregate = (props) => {

  console.log('DASHBOARDAGGREGATE', props.investments);
  return (
    <div>
    {props.investments &&
        <div className="aggregate-row row">
          <div className="dash-total-invested col-sm-4">
            <div className="dash-agg-item">
            <h4>Total Invested</h4>

              <p>{props.investments.totals ? (Math.round(props.investments.totals.total_invested * 100) / 100).toFixed(2): <span>Loading</span>}</p>
            </div>
          </div>
          <div className="dash-total-value col-sm-4">
          <div className="dash-agg-item">
            <h4>Current Value</h4>
            <p>{props.investments.totals ? (Math.round(props.investments.totals.current_value  * 100) / 100).toFixed(2): <span>Loading</span>}</p>
          </div>
          </div>
          <div className="dash-difference col-sm-4">
            <div className="dash-agg-item">
                <h4>Profit/Loss</h4>
                <p>{props.investments.totals ? (Math.round(props.investments.totals.difference  * 100) / 100).toFixed(2): <span>Loading</span>}</p>
            </div>
          </div>
        </div>

    }
    </div>
  )
}



export default DashboardAggregate;
