import React, { Component } from 'react';
import Modal from 'react-modal';


class DashboardModal extends Component {

  constructor(props) {
    super(props);
    this.handleAddTrade = this.handleAddTrade.bind(this);
  }

  handleAddTrade(e) {
    e.preventDefault();
    const name = this.props.selectedCoin.name;
    const price = e.target.elements.price.value.trim();
    const amount = e.target.elements.amount.value.trim();


      //start action creator
      this.props.onFormSubmit({name, price, amount});

      //call handleClearSelectedCoin to close the modal
      this.props.handleClearSelectedCoin();
  }

  render() {
    return (
      <Modal
        isOpen={!!this.props.selectedCoin}
        contentLabel="Add an Investment"
        ariaHideApp={false}
        onRequestClose={this.props.handleClearSelectedCoin}
      >
        <h3>
          Add a {this.props.selectedCoin && this.props.selectedCoin.name} Investment to Track
        </h3>
        <form onSubmit={this.handleAddTrade}>
          <div className="form-group">
            <label>Amount Purchased</label>
            <input type="number" step="0.000001" className="form-control" name="amount" placeholder="Amount Purchased" />
          </div>
          <div className="form-group">
            <label>Price</label>
            <input type="number" step="0.01" className="form-control" name="price" placeholder="Purchase Price(USD)" />
          </div>
          <button type="submit" className="btn btn-primary">Submit</button>
        </form>
        <button onClick={this.props.handleClearSelectedCoin} className="btn btn-danger">Cancel</button>
      </Modal>
    )
  }
}

export default DashboardModal;
