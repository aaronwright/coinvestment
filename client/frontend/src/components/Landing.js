import React from 'react';


 const Landing = () => {
    return (
        <div style={{textAlign: 'center'}}>
            <h1>
                COINVESTMENT
            </h1>
            <p>
                Keep track of your crypto investments
            </p>
        </div>
    );
}


export default Landing;
