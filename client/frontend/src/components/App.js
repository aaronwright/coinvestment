import React, { Component } from 'react';
import { BrowserRouter, Route, Redirect, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../actions';


import Header from './Header';
import Landing from './Landing';
import Login from './Login';
import Dashboard from './Dashboard';



class App extends Component {

    constructor() {
        super();
        this.isAuthed = this.isAuthed.bind(this);
    }


    componentDidMount() {
      // this.props.fetchUser(this.props.auth.token);

      this.props.getAllCoins();
    }

    isAuthed() {
      if (localStorage.getItem('access_token')) {
          if(this.props.auth) {
            return true;
          } else {
            this.props.fetchUser();
            return false;
          }
      } else {
        return false;
      }
    }

    render() {
        return (
        <div>
          <BrowserRouter>
            <div>
              <Header />
              <Switch>
              <Route exact path="/" component={Landing} />
              <AlreadyLoggedInRoute exact authed={this.isAuthed()} path='/login' component={Login} />
              <PrivateRoute exact authed={this.isAuthed()} path='/dashboard' component={Dashboard} />
              </Switch>
            </div>
          </BrowserRouter>
        </div>
        );
    };
}

const mapStateToProps = state => {
  return {
    auth: state.auth,
    token: state.token.token
  };
}



function PrivateRoute ({component: Component, authed}) {
  return (
    <Route
      render={(props) => authed === true
        ? <Component {...props} />
        : <Redirect to={{pathname: '/login'}} />}
    />
  )
}


function AlreadyLoggedInRoute ({component: Component, authed}) {
  return (
    <Route
      render={(props) => authed === true
        ? <Redirect to={{pathname: '/dashboard'}} />
        : <Component {...props} />}
    />
  )
}

export default connect(mapStateToProps, actions)(App);
