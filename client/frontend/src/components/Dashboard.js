import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import DashboardModal from './DashboardModal';
import DashboardAggregate from './DashboardAggregate';

class Dashboard extends Component {
        constructor(props) {
          super(props);
          this.renderInvestmentList = this.renderInvestmentList.bind(this);
          this.renderCoinList = this.renderCoinList.bind(this);
          this.onShowForm = this.onShowForm.bind(this);
          this.onFormSubmit = this.onFormSubmit.bind(this);
          this.handleClearSelectedCoin = this.handleClearSelectedCoin.bind(this);

          this.state = {
            selectedCoin: undefined
          }
        }

        componentDidMount() {
          this.props.getInvestments();
        }

        renderInvestmentList() {
            if (this.props.investments) {
              console.log(this.props.investments);

              return this.props.investments.coins.map((coin) => {
                return (
                  <div className="dash-list-investment" key={coin.id}>
                    <span>{coin.name}</span>
                    <span>
                      <span className="dash-list-sm-txt">AMT</span> {coin.amount}
                    </span>
                    <span>
                    <span className="dash-list-sm-txt">VAL </span>{(Math.round(coin.value * 100) / 100).toFixed(2)}
                    </span>
                    <span className="dash-list-sm-txt">
                      edit
                    </span>
                  </div>
                );
              });
            } else {
              return <p>Nothing here...</p>;
            }
        }

        renderCoinList() {
          if (this.props.coins) {
              return this.props.coins.map((coin) => {
                  return (
                    <div key={coin.name} className="dash-list-coin">
                        <span>{coin.name}</span>
                        <span><span className="dash-list-sm-txt">Current Price</span>{coin.current_price}</span>
                        <span onClick={() => this.onShowForm(coin)}>+</span>
                  </div>
                  )
              })

          }
        }

        renderTradeList() {
          if (this.props.investments) {
              return this.props.investments.allTrades.map((trade) => {
                  return (
                    <div key={trade.id} className="dash-list-trade">
                        <span>{trade.coin_name}</span>
                        <span><span className="dash-list-sm-txt">Profit/Loss</span>{(Math.round(trade.difference * 100) / 100).toFixed(2)}</span>
                        <span><span className="dash-list-sm-txt">Total Val</span>{(Math.round(trade.current_val * 100) / 100).toFixed(2)}</span>
                  </div>
                  )
              })

          }

        }


        onShowForm(coin) {
          this.setState({selectedCoin: coin});
        }

        onFormSubmit(data) {
          //set off AddTrade action creator with data
          this.props.addTrade(data);
        }

        handleClearSelectedCoin = () => {
            this.setState({selectedCoin: undefined});
        }
        render() {
          return (
              <div className="container">
                  <DashboardAggregate investments={this.props.investments} />
                  <div className="row">
                    <div className="col-sm-5">
                      <div className="dash-list-container">
                        <div className="list-container-header">
                          <h3>Your Investments</h3>
                        </div>
                        {this.renderInvestmentList()}
                      </div>
                      <div className="dash-list-container">
                      <div className="list-container-header">
                        <h3>All Trades</h3>
                      </div>
                      {this.renderTradeList()}
                      </div>
                    </div>
                    <div className="col-sm-7">
                      <div className="dash-list-container">
                        <div className="list-container-header">
                          <h3>All Coins</h3>
                        </div>
                        {this.renderCoinList()}
                      </div>
                    </div>
                  </div>
                  <DashboardModal handleClearSelectedCoin={this.handleClearSelectedCoin} onFormSubmit={this.onFormSubmit} selectedCoin={this.state.selectedCoin} />
              </div>
          );
      }
}

function mapStateToProps(state) {
    return {
        auth: state.auth,
        token: state.token,
        investments: state.investments.investments,
        coins: state.coins.coins
          };
}

export default connect(mapStateToProps, actions)(Dashboard);
