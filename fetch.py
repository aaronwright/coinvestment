import sqlite3


connection = sqlite3.connect('data.db')

cursor = connection.cursor()
# user 

user = (1, 'Aaron', 'asdf')


# coins 

btc = (1, 'Bitcoin', 6201.42, 'The most popular cryptocurrency out there.')
ltc = (2, 'Litecoin', 55.98, 'The younger sibling of bitcoin.')
xmr = (3, 'Monero', 115.72, 'The top privacy coin')


#investments 
inv = (1, 1.54236, 5932.32, 1, 1)



insert_coins_query = "INSERT INTO coins VALUES (?, ?, ?, ?)"
insert_user_query = "INSERT INTO users VALUES (?, ?, ?)"
insert_inv_query = "INSERT INTO investments VALUES (?, ?, ?, ?, ?)"


cursor.execute(insert_coins_query, btc)
cursor.execute(insert_coins_query, ltc)
cursor.execute(insert_coins_query, xmr)
cursor.execute(insert_user_query, user)
cursor.execute(insert_inv_query, inv)


connection.commit()

connection.close()